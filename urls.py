from django.conf.urls.defaults import patterns, include, url
from destination.views.main_frame import main_frame_view
from destination.views.user import new_user_view, user_login, user_logout
from destination.views.been_area import been_view, add_view, delete_view
from destination.views.wish_area import been_view as wish_been_view, add_view as wish_add_view, delete_view as wish_delete_view
from destination.views.tags import tags_view
# Uncomment the next two lines to enable the admin:
from django.conf import settings

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
  #  url(r'^(\w+)/(\w+)','index',name=('home','name')),
    #url(r'^api/v1/places','places'),
    #url(r'^api/vi/places/(\d+)','place',name='id'),
#    url(r'^$','rest.views.index'),
#    url(r'^api/v1/',include('rest.urls')),
    url(r'^been_area/get/$', been_view),
    url(r'^been_area/add/$', add_view),
    url(r'^been_area/delete/$', delete_view),
    url(r'^wish_area/get/$', wish_been_view),
    url(r'^wish_area/add/$', wish_add_view),
    url(r'^wish_area/delete/$', wish_delete_view),
    url(r'^login/$', user_login),
    url(r'^logout/$', user_logout),
     url(r'^test/$', main_frame_view),
     url(r'^new_user/$', new_user_view),
     url(r'^tags/$', tags_view),
    url(r'^$', main_frame_view),
    # url(r'^restdemo/', include('restdemo.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
     url(r'^admin/', include(admin.site.urls)),
)
if settings.DEBUG:
    urlpatterns += patterns('',
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
            'document_root': settings.MEDIA_ROOT,
            }),
    )
