#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'yupbank'

from django.db import models
from base import Modify, Tag, SuggestMonth, Transport, PositionLocationBase, PositionTimeBase
from django.contrib.auth.models import User
from destination.cache._cache import get_cache
#from scene import Scene


class Area(PositionLocationBase, PositionTimeBase):
    key = 'area_tag_set'

    def delete(self, using=None):
        self.clean_cache()
        if self.scene_set:
            for scene in self.scene_set.all():
                scene.delete()
        return super(Area, self).delete(using)

    @classmethod
    def get_all_tags(cls):
        cache = get_cache()
        value = cache.smembers(cls.key)
        if value:
            return value
        for i in Tag.objects.filter(area__isnull=False):
            cache.sadd(cls.key, i.id)
        return cache.smembers(cls.key)

    @classmethod
    def get_name_by_ids(cls, ids):
        tags = cls.objects.get(pk__in=ids)
        return [[i.ids, i.tag] for i in tags]

    def clean_cache(self):
        cache = get_cache()
        cache.delete(self.key)

    def get_near(self, num=10):
        return self.lat, self.lng

    def save(self, force_insert=False, force_update=False, using=None):
        self.clean_cache()
        return super(Area, self).save(force_insert=force_insert, force_update=force_update, using=using)


    class Meta(object):
        ordering = ['full_name']
        db_table = 'destination_area'
        app_label = 'destination'
        verbose_name = u'景区'
        ordering = ['modify']
