#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'yupbank'

from django.db import models
from base import PositionLocationBase
from area import Area


class Scene(PositionLocationBase):
    area = models.ForeignKey(Area, blank=True, null=True, verbose_name=u'景区')

    class Meta(object):
        db_table = 'destination_scene'
        app_label = 'destination'
        verbose_name = u'景点'

