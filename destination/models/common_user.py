#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.db import models
from area import Area
from hashlib import sha256
from settings import USER_ID_START, ACCOUNT_CID, DEFAULT_CID
from destination.cache._cache import get_cache


def hash_password(user_id, password):
    return sha256('%s%s'%(user_id, password)).hexdigest()

def tee(func):
    def _(*args, **kwargs):
        print args, kwargs
        print func
        res = func(*args, **kwargs)
        print res
    return _

class AuthStorage(models.Model):
    user_id = models.IntegerField()
    cid = models.IntegerField()
    token = models.TextField()
    nick_name = models.TextField()

    class Meta(object):
        db_table = 'destination_auth_storage'
        app_label = 'destination'
        verbose_name = u'授权用户'



class UserMail(models.Model):
    user_id = models.IntegerField()
    mail = models.EmailField()

    @classmethod
    def exists(cls, mail):
        return cls.objects.filter(mail=mail).exists()

    class Meta(object):
        db_table = 'destination_user_mail'
        app_label = 'destination'
        verbose_name = u'用户邮箱'



class User(models.Model):
    user_id = models.IntegerField()
    cid = models.IntegerField()
    state = models.IntegerField(default=1)



    @classmethod
    def new(cls, cid):
        user = cls.objects.order_by('-user_id')
        if user.count():
            user_id=user[0].user_id + 1
        else:
            user_id=USER_ID_START

        user = User(user_id=user_id, cid=cid)
        user.save()
        return user

    class Meta(object):
        db_table = 'destination_user'
        app_label = 'destination'
        verbose_name = u'新建用户'

class UserPassword(models.Model):
    user_id = models.IntegerField()
    password = models.CharField(max_length=256)

    @staticmethod
    def verify_by_user_id(user_id, password):
        try:
            up = UserPassword.objects.get(user_id=user_id)
        except Exception,e:
            return False
        if up.password == hash_password(user_id, password):
            return up.user_id

    @classmethod
    def new(cls, user_id, password):
        up = cls(user_id=user_id, password=hash_password(user_id, password))
        up.save()
        return up

    class Meta(object):
        db_table = 'destination_user_password'
        app_label = 'destination'
        verbose_name = u'用户密码'



class WishArea(models.Model):
    user = models.ForeignKey(User)
    area = models.ManyToManyField(Area)

    key_prefix = 'wish_area:%s'
    cache = get_cache()

    def add_wish_area(self, area):
        self.cache.sadd(self.key_prefix%self.user.user_id, area.id)
        return self.area.add(area)

    def get_wish_area(self):
        return self.cache.smembers(self.key_prefix%self.user.user_id)

    def delete_wish_area(self, area):
        self.cache.srem(self.key_prefix%self.user.user_id, area.id)
        return self.area.remove(area)


    class Meta(object):
        db_table = 'destination_wish_area'
        app_label = 'destination'
        verbose_name = u'想去的地方'

class BeenArea(models.Model):
    user = models.ForeignKey(User)
    area = models.ManyToManyField(Area)

    key_prefix = 'been_area:%s'
    cache = get_cache()

    def add_been_area(self, area):
        self.cache.sadd(self.key_prefix%self.user.user_id, area.id)
        return self.area.add(area)

    def get_been_area(self):
        return self.cache.smembers(self.key_prefix%self.user.user_id)

    def delete_been_area(self, area):
        self.cache.srem(self.key_prefix%self.user.user_id, area.id)
        return self.area.remove(area)


class Meta(object):
        db_table = 'destination_been_area'
        app_label = 'destination'
        verbose_name = u'去过的地方'


#@tee
def new_user_default(cid, login_name, login_pwd, **kwargs):
    if UserMail.exists(login_name):
        user_mail = UserMail.objects.get(mail=login_name)
        u =User.objects.get(user_id=user_mail.user_id)
        if UserPassword.verify_by_user_id(u.user_id, login_pwd):
            return u
    else:
        user = User.new(cid=DEFAULT_CID)
        user_mail = UserMail(user_id=user.user_id, mail=login_name)
        user_mail.save()
        UserPassword.new(user_id=user.user_id, password=login_pwd)
        return user


def new_user_auth(cid, nick_name, token, **kwargs):
    if AuthStorage.objects.filter(cid=cid, nick_name=nick_name).exists():
        auth_storage = AuthStorage.objects.get(cid=cid, nick_name=nick_name)
        return User.objects.get(user_id=auth_storage.user_id)
    else:
        user = User.new(cid=cid)
        auth_storage = AuthStorage(user_id=user.user_id, cid=cid, token=token, nick_name=nick_name)
        auth_storage.save()
        return user

def new_user(cid, *args, **kwargs):
    if cid in ACCOUNT_CID:
        t = new_user_auth(cid, *args, **kwargs)
    else:
        t = new_user_default(cid, *args, **kwargs)
    return t

def login_user_default(cid, login_name, login_pwd, **kwargs):
    user_mail = UserMail.objects.get(mail=login_name, cid=cid)
    if user_mail:
        return UserPassword.verify_by_user_id(user_id=user_mail.user_id, password=login_pwd)


def login_user_auth(cid, uid, access_token, **kwargs):
    auth_storage = AuthStorage.objects.get(cid=cid, nick_name=uid, token=access_token)
    if auth_storage:
        return auth_storage.user_id


def login(cid, *args, **kwargs):
    if cid in ACCOUNT_CID:
        return login_user_auth(cid, *args, **kwargs)
    else:
        return login_user_default(cid, *args, **kwargs)




