#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'yupbank'

from django.db import models
from base import PositionNameBase, PositionTimeBase
from place import Place




class Line(PositionNameBase, PositionTimeBase):
    places = models.ManyToManyField(Place, null=True, blank=True, verbose_name='地点列表')
    location = models.BigIntegerField(default='')
    province_id = models.BigIntegerField(blank=True, null=True, verbose_name=u'省')
    city_id = models.BigIntegerField(blank=True, null=True, verbose_name=u'市')
    location_name = models.CharField(max_length=500, default='', blank=True, verbose_name=u'行政区划')


    class Meta(object):
        db_table = 'destination_line'
        app_label = 'destination'
        verbose_name = u'线路'
        ordering = ['modify']




