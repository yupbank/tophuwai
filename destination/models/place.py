#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'yupbank'

from base import PositionLocationBase, PositionTimeBase


class Place(PositionLocationBase, PositionTimeBase):
    def delete(self, using=None):
        if self.line_set:
            for line in self.line_set.all():
                if line.places.count() == 1:
                    line.delete()
                else:
                    line.places.remove(self)
        return super(Place, self).delete(using)

    class Meta(object):
        db_table = 'destination_place'
        app_label = 'destination'
        verbose_name = u'地名'
        ordering = ['modify']

