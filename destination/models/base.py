#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
location_base.py
Author: yupbank
Email:  yupbank@gmail.com

Created on
2012-07-07
'''
from django.contrib.auth.models import User
from django.db import models
from destination.util import pid_city, pid_province, place_name
from destination.util import get_lat_lng_range

class Modify(models.Model):
    modify_time = models.DateTimeField(auto_now_add=True, verbose_name=u'修改时间')
    modify_user = models.ForeignKey(User)


    class Meta(object):
        db_table = 'destination_modify'
        app_label = 'destination'
        verbose_name = u'修改'
        ordering = ['-modify_time']

    def __unicode__(self):
        return '%s-%s'%(self.modify_user, self.modify_time)



class Tag(models.Model):
    tag = models.CharField(max_length=100, verbose_name=u'名称')
    create_time = models.DateTimeField(auto_now=True, verbose_name=u'创建时间')
    creater = models.ForeignKey(User)

    @classmethod
    def get_or_create_by_name(cls, tag, user):
        tag = tag.strip()
        obj = cls.objects.filter(tag=tag)
        if obj:
            obj = obj[0]
        else:
            obj = cls()
            obj.tag = tag
            obj.creater = user
            obj.save()

        return obj

    def __unicode__(self):
        return self.tag

    class Meta(object):
        db_table = 'destination_tag'
        app_label = 'destination'
        verbose_name = u'主题'

class SuggestMonth(models.Model):
    name = models.CharField(max_length=200)
    value = models.IntegerField()

    def __unicode__(self):
        return self.name

    class Meta(object):
        db_table = 'destination_suggest_month'
        app_label = 'destination'
        verbose_name = u'最佳观赏时间'

class Transport(models.Model):
    name = models.CharField(max_length=200)
    value = models.IntegerField()

    def __unicode__(self):
        return self.name

    class Meta(object):
        db_table = 'destination_transport'
        app_label = 'destination'
        verbose_name = u'交通方式'

class PositionNameBase(models.Model):
    full_name = models.CharField(max_length=100, default='', verbose_name=u'全称')
    short_name = models.CharField(max_length=50, blank=True, default='', verbose_name=u'简称')
    alternative_name = models.CharField(max_length=100, blank=True, default='', verbose_name=u'别名')

    tags = models.ManyToManyField(Tag, null=True, blank=True, verbose_name=u'主题')
    create_time = models.DateTimeField(auto_now_add=True, verbose_name=u'创建时间')
    creater = models.ForeignKey(User)
    modify = models.ForeignKey(Modify)
    hot = models.IntegerField(choices=(
        (1, u'一星'),
        (2, u'二星'),
        (3, u'三星'),
        (4, u'四星'),
        (5, u'五星'),
        ), verbose_name=u'星级' )

    image = models.ImageField(upload_to = 'upload', default='', null=True, blank=True, verbose_name=u'照片')

    desc = models.TextField(default='',blank=True, verbose_name=u'简介')

    remark = models.TextField(default='',blank=True, verbose_name=u'备注')

    def terminate(self):
        value = dict()
        for i,j in self.__dict__.iteritems():
            if isinstance(i, str):
                if not i.startswith('_') and i <> 'id':
                    value[i] = j
            else:
                value[i] = j
        return value

    
    def __unicode__(self):
            return self.full_name

    class Meta(object):
        abstract = True

class PositionLocationBase(PositionNameBase):
    location = models.BigIntegerField()
    province_id = models.BigIntegerField(blank=True, null=True, verbose_name=u'省')
    city_id = models.BigIntegerField(blank=True, null=True, verbose_name=u'市')
    location_name = models.CharField(max_length=500, default='', blank=True, verbose_name=u'行政区划')
    lat = models.FloatField(db_index=True, blank=True, null=True, default='', verbose_name=u'经度')
    lng = models.FloatField(db_index=True, blank=True, null=True, default='', verbose_name=u'维度')
    altitude = models.FloatField(blank=True, null=True, default=0, verbose_name=u'海拔')
    
    def get_near(self, lat, lng, distance=1.0):
        lat1, lat2, lng1, lng2 = get_lat_lng_range(lat, lng, distance) 
        res = self.filter(lat__lt=lat2).filter(lat__gt=lat1).filter(lng__lt=lng2).filter(lng__gt=lng1)
        return res 

    def save(self, force_insert=False, force_update=False, using=None):
        if hasattr(self, 'location'):
            self.province_id = pid_province(self.location)
            self.city_id = pid_city(self.location) if not self.province_id == pid_city(self.location) else self.location

            if not hasattr(self, 'location_name') or len(self.location_name)<2:
                province_name = place_name(pid_province(self.location))
                city_name = place_name(pid_city(self.location)).replace(province_name, '').strip()
                self.location_name = '%s%s'%(place_name(pid_city(self.location)).replace(' ', '-'), place_name(self.location).replace(province_name, '').replace(city_name, ''))
        return PositionNameBase.save(self, force_insert, force_update, using)


    class Meta(object):
        abstract = True


class PositionTimeBase(models.Model):
    suggest_month = models.ManyToManyField(SuggestMonth, verbose_name=u'最佳观赏')
    suggest_time = models.IntegerField(choices=(
        (1, u'随便看看'),
        (2, u'半天'),
        (3, u'1天'),
        (4, u'2天'),
        (5, u'3天'),
        (6, u'4天'),
        (7, u'5天'),
        (8, u'6天'),
        (9, u'7天'),
        (10, u'8天'),
        (11, u'9天'),
        (12, u'10天'),
        (13, u'10天以上')),
        verbose_name=u'建议时间')
    transport = models.ManyToManyField(Transport, verbose_name=u'交通方式')



    class Meta(object):
        abstract = True






