#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
province.py
Author: yupbank
Email:  yupbank@gmail.com

Created on
2012-07-18
'''
from django.db import models
from place import Place
from area import Area
from line import Line
from base import Modify
from destination.util import pid_province


class Province(models.Model):
    name = models.CharField(max_length=200, verbose_name=u'名称')
    value = models.FloatField()
    short_name = models.CharField(max_length=200, blank=True, default='', verbose_name=u'简称')
    places = models.ManyToManyField(Place, blank=True)
    places_count = models.IntegerField(blank=True, null=True)
    areas = models.ManyToManyField(Area, blank=True)
    areas_count = models.IntegerField(blank=True, null=True)
    lines = models.ManyToManyField(Line, blank=True)
    lines_count = models.IntegerField(blank=True, null=True)
    modify = models.ForeignKey(Modify, blank=True, null=True)


    def update_count(self):
        self.places_count = self.places.count()
        self.areas_count = self.areas.count()
        self.lines_count = self.lines.count()
        self.save()

    @classmethod
    def remove_place(cls, place):
        for i in cls.objects.all():
            i.places.remove(place)
            i.save()


    @classmethod
    def add_place(cls, place):
        province_id = pid_province(place.location)
        if province_id:
            province = cls.objects.get(value=province_id)
            province.places.add(place)
            province.save()


    @classmethod
    def remove_line(cls, line):
        for i in cls.objects.all():
            i.lines.remove(line)
            i.save()

    @classmethod
    def remove_area(cls, area):
        for i in cls.objects.all():
            i.areas.remove(area)
            i.save()




    class Meta(object):
        db_table = 'destination_province'
        app_label = 'destination'
        verbose_name = u'省市'

    def __unicode__(self):
        return self.name
