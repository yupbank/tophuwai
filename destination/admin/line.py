#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
line.py
Author: yupbank
Email:  yupbank@gmail.com

Created on
2012-07-07
'''
from django.contrib import admin
from django.conf.urls.defaults import patterns
from base import BaseAdmin, TranSugBase
from django.http import HttpResponse
from destination.models import Place, Line
from django.contrib.admin.util import unquote


import json

class LineAdmin(TranSugBase):
    fields = ['suggest_month','location','full_name', 'short_name', 'alternative_name',\
              'hot', 'image', 'desc', 'remark',\
               'suggest_time',]
    list_display = ['id', 'name', 'tag','relate_place', 'hot', 'my_modify','create', 'operation']

    def relate_place(self, obj):
        count = obj.places.count()
        if count:
            return """
                <a href="../place/?line=%s">%s地名</a>
                """%(obj.id, count)
        else:
            return """0地名
            """
    relate_place.short_description = u'关联地名'
    relate_place.allow_tags = True

    def name(self, obj):
        return """
        %s<br />%s<br />%s
        """%(obj.full_name, obj.short_name or '-', obj.alternative_name or '-')
    name.short_description = u'全称、简称、别名'
    name.allow_tags = True
    name.admin_order_field = 'full_name'


    def tag(self, obj):
        tags = obj.tags.all()
        tags = ['<a href="?tags=%s">%s</a>'%(i.id, i.tag) for i in tags]
        return """
            %s
            """%(','.join(tags))
    tag.short_description = u'主题'
    tag.allow_tags = True

    def my_modify(self, obj):
        return """
        %s<br />%s
        """%(str(obj.modify.modify_time), obj.modify.modify_user)
    my_modify.short_description = u'最后修改'
    my_modify.allow_tags = True
    my_modify.admin_order_field = 'modify'

    def create(self, obj):
        return """
        %s<br />%s
        """%(str(obj.create_time), obj.creater)
    create.short_description = u'添加日期'
    create.allow_tags = True
    create.admin_order_field = 'create_time'

    def operation(self, obj):
        return """
        <a href="./%s/" target="_blank">编辑</a>
        """%obj.id
    operation.short_description = u'操作'
    operation.allow_tags = True

    def get_urls(self):
        urls = super(LineAdmin, self).get_urls()
        my_urls = patterns('',
            (r'^api/$', self.api_view),
        )
        return my_urls + urls

    def api_view(self, request):
        key = request.GET.get('key', None)
        if key:
            res = Place.objects.filter(full_name__icontains=key)
            if res:
                res = [(i.id, i.full_name, (i.lat, i.lng)) for i in res]
                return HttpResponse(json.dumps(res))
            
        return HttpResponse(json.dumps({
            'status': 0,
            'msg': u'error'
        }))
    
    def change_view(self, request, object_id, extra_context={}):
        obj = self.get_object(request, unquote(object_id))
        places = obj.places.all()
        places_info = []
        for place in places:
            places_info.append([place.id, place.full_name, [place.lat, place.lng]])
        extra_context.update(dict(places=json.dumps(places_info)))
        return TranSugBase.change_view(self, request, object_id, extra_context)

    def save_model(self, request, obj, form, change):
        s = TranSugBase.save_model(self, request, obj, form, change)
        place_info = request.POST.get('places', [])
        place_info = json.loads(place_info)
        #print 'sadadas', place_info, '!!!', request.POST
        if place_info:
            place_info = place_info
        obj.places.clear()
        for place in place_info:
            p = Place.objects.get(id=place[0])
            obj.places.add(p)
            obj.save()
        return s
