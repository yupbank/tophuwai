#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
province.py
Author: yupbank
Email:  yupbank@gmail.com

Created on
2012-07-18
'''

from django.contrib import admin

class ProvinceAdmin(admin.ModelAdmin):
    actions_on_top = False
    actions_on_bottom = True
    list_display = ['id', 'full_name', 'relate_place', 'relate_area', 'relate_line', 'my_modify']
    fields = ['name', 'short_name']
    search_fields = ['name']

    def full_name(self, obj):
        return """
        %s-<br />%s
        """%(obj.name, obj.short_name)
    full_name.short_description = u'名称、简称'
    full_name.allow_tags = True

    def relate_place(self, obj):
        obj.update_count()
        if obj.places_count:
            return """
            <a href="../place/?province_id=%s">%s地点</a>
            """%(int(obj.value), obj.places_count)

        else:
            return """
            0地点
            """
    relate_place.short_description = u'关联地点'
    relate_place.allow_tags = True
    relate_place.admin_order_field = 'places_count'

    def my_modify(self, obj):
        return """
        %s<br />%s
        """%(str(obj.modify.modify_time), obj.modify.modify_user)
    my_modify.short_description = u'最后修改'
    my_modify.allow_tags = True
    my_modify.admin_order_field = 'modify'

    def relate_area(self, obj):
        if obj.areas_count:
            return """
            <a href="../area/?province_id=%s">%s景区</a>
            """%(int(obj.value), obj.areas_count)
        else:
            return """
            0景区
            """

    relate_area.short_description = u'关联景区'
    relate_area.allow_tags = True
    relate_area.admin_order_field = 'areas_count'

    def relate_line(self, obj):
        if obj.lines_count:
            return """
            <a href=../line/?province_id=%s>%s线路</a>
            """%(int(obj.value), obj.lines_count)
        else:
            return """
            0线路
            """
    relate_line.short_description = u'关联线路'
    relate_line.allow_tags = True
    relate_line.admin_order_field = 'lines_count'


