#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.contrib import admin
from destination.models import Place, Area, Line

class TagAdmin(admin.ModelAdmin):
    actions = []
    list_display = ['id', 'tag', 'relate_place', 'relate_area', 'relate_line', 'create']
    actions_on_top = False
    actions_on_bottom = True
    search_fields = ['tag']

    def relate_place(self, obj):
        place = Place.objects.filter(tags__in=[obj])
        if place.count():
            return """
            <a href="../place/?tags=%s">%s地点</a>
            """%(obj.id, place.count())
        else:
            return "0地点"
    relate_place.allow_tags = True
    relate_place.short_description = u'关联地名'

    def relate_area(self, obj):
        area = Area.objects.filter(tags__in=[obj])
        if area.count():
            return """
            <a href="../area/?tags=%s">%s景区</a>
            """%(obj.id, area.count())
        else:
            return "0景区"
    relate_area.allow_tags = True
    relate_area.short_description = u'关联景区'

    def relate_line(self, obj):
        line = Line.objects.filter(tags__in=[obj])
        if line.count():
            return """
            <a href="../line/?tags=%s">%s线路</a>
            """%(obj.id, line.count())
        else:
            return "0线路"
    relate_line.short_description = u'关联线路'
    relate_line.allow_tags = True

    def create(self, obj):
        return """
        %s<br />%s
        """%(str(obj.create_time), obj.creater)
    create.short_description = u'添加日期'
    create.allow_tags = True
    create.admin_order_field = 'create_time'

