#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'yupbank'
from django.contrib import admin
from .base import BaseAdmin
from destination.models import Area, Scene
import json
from django.http import Http404, HttpResponse, HttpResponseRedirect



class SceneAdmin(BaseAdmin):
    search_fields = ['full_name']
    list_display = ['id', 'full_name', 'gps', 'tag', 'hot', 'my_modify', 'create', 'operation']
    fields = ['location','full_name', 'short_name', 'alternative_name',\
              'hot', 'image', 'desc', 'remark',\
              'lat', 'lng', 'area', 'altitude']


    def changelist_view(self, request, extra_context={}):
        area_id = request.GET.get('area')
        if area_id:
            area = Area.objects.get(id=area_id)
            objs = Scene.objects.filter(area=area)
            if objs:
                points = [[obj.id, obj.full_name, [obj.lat, obj.lng]] for obj in objs]
            else:
                points = []
            extra_context = {
                'points': json.dumps(points),
                'area_id': area_id
            }
        return super(SceneAdmin, self).changelist_view(request, extra_context)

    def gps(self, obj):
        return """
        %s,<br />%s,<br />%s
        """%(obj.lat, obj.lng, obj.altitude)
    gps.short_description = u'GPS'
    gps.allow_tags = True

    def tag(self, obj):
        tags = obj.tags.all()
        tags = ['<a href="?tags=%s">%s</a>'%(i.id, i.tag) for i in tags]
        return """
            %s
            """%(','.join(tags))
    tag.short_description = u'主题'
    tag.allow_tags = True

    def my_modify(self, obj):
        return """
        %s<br />%s
        """%(str(obj.modify.modify_time), obj.modify.modify_user)
    my_modify.short_description = u'最后修改'
    my_modify.allow_tags = True
    my_modify.admin_order_field = 'modify'

    def create(self, obj):
        return """
        %s<br />%s
            """%(str(obj.create_time), obj.creater)
    create.short_description = u'添加日期'
    create.allow_tags = True
    create.admin_order_field = 'create_time'

    def operation(self, obj):
        return """
        <a href="./%s/">编辑</a>
        """%obj.id
    operation.short_description = u'操作'
    operation.allow_tags = True

    def response_add(self, request, obj, post_url_continue='../%s/'):
        #post_url_continue = '../%%s/?area=%s'%obj.area.id
        #return BaseAdmin.response_add(self, request, obj, post_url_continue)
        return HttpResponseRedirect('../../area/?id=%s'%obj.area.id)

    def response_change(self, request, obj):
        return HttpResponseRedirect('../../area/?id=%s'%obj.area.id)
