#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
__init__.py
Author: yupbank
Email:  yupbank@gmail.com

Created on
2012-07-07
'''

from django.contrib import admin
from destination.models import Tag, Place, Area, Scene, Line, SuggestMonth, Transport, Province
from django.contrib.sites.models import Site
from .line import LineAdmin
from .area import AreaAdmin
from .place import PlaceAdmin
from .scene import SceneAdmin
from .tag import TagAdmin
from .province import ProvinceAdmin


#admin.site.register(Place)
#admin.site.register(Tag)
admin.site.register(Place, PlaceAdmin)
admin.site.register(Province, ProvinceAdmin)
admin.site.register(Area, AreaAdmin)
admin.site.register(Scene, SceneAdmin)
admin.site.register(Line, LineAdmin)
admin.site.register(SuggestMonth)
admin.site.register(Transport)
admin.site.register(Tag, TagAdmin)
#admin.site.register(Line, LineAdmin)
admin.site.unregister(Site)

