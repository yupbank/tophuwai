# coding: utf-8

from destination.models import Place, Area, Province
from destination.util import pid_city, pid_province, place_name, place_full_name
from base import TranSugBase

class PlaceAdmin(TranSugBase):
    search_fields = ['full_name', 'suggest_time', 'short_name', 'alternative_name', 'location_name']
    list_display = ['id', 'name', 'division','gps', 'tag','hot', 'create', 'my_modify', 'operation']
    fields = ['suggest_month','location','full_name', 'short_name', 'alternative_name', \
              'hot', 'image', 'desc', 'remark', \
              'lat', 'lng', 'altitude', 'suggest_time','transport']
    list_display_links = []
    actions = ['default_action', 'change_into_area']

    #form = ChannelForm
    model = Place
    list_per_page = 50
    list_display_link = ['operation']

    def change_into_area(self, request, queryset):
        for obj in queryset:
            obj_info = obj.terminate()
            a = Area(**obj_info)
            if not Area.objects.filter(full_name=a.full_name).exists():
                a.save()
                for tag in obj.tags.all():
                    a.tags.add(tag)
                for suggest_month in obj.suggest_month.all():
                    a.suggest_month.add(suggest_month)
                for transport in obj.transport.all():
                    a.transport.add(transport)
                a.save()

                self.save_province(a)
            pro = obj.province_set.all()
            if pro:
                pro = pro[0]
                pro.places.remove(obj)
            #Province.remove_place(obj)
            obj.delete()

    def operation(self, obj):
        return """
        <a target="_blank" href="./%s/">编辑</a>
        """%obj.id
    operation.short_description = u'操作'
    operation.allow_tags = True

    def gps(self, obj):
        return """
        %s,<br />%s,<br />%s
        """%(obj.lat, obj.lng, obj.altitude)
    gps.short_description = u'GPS'
    gps.allow_tags = True

    def tag(self, obj):
        tags = obj.tags.all()
        tags = ['<a href="?tags=%s">%s</a>'%(i.id, i.tag) for i in tags]
        return """
        %s
        """%(','.join(tags))
    tag.short_description = u'主题'
    tag.allow_tags = True

    def create(self, obj):
        return """
        %s<br />%s
        """%(str(obj.create_time), obj.creater)
    create.short_description = u'添加日期'
    create.allow_tags = True
    create.admin_order_field = 'create_time'

    def name(self, obj):
        return """
        %s<br />%s<br />%s
        """%(obj.full_name, obj.short_name or '-', obj.alternative_name or '-')
    name.short_description = u'全称、简称、别名'
    name.allow_tags = True
    name.admin_order_field = 'full_name'


    def division(self, obj):
        province_name = place_name(pid_province(obj.location))
        city_name = place_name(pid_city(obj.location)).replace(province_name, '').strip()
        return """<strong>%s</strong><br />
            %s
            """%(province_name,  place_full_name(obj.location).replace(province_name+'-', ''))

    division.short_description = u'行政区划'
    division.allow_tags = True

    def my_modify(self, obj):
        return """
        %s<br />%s
        """%(str(obj.modify.modify_time), obj.modify.modify_user)
    my_modify.short_description = u'最后修改'
    my_modify.allow_tags = True
    my_modify.admin_order_field = 'modify'

    def add_view(self, request, form_url='', extra_context={}):
        location = request.POST.get('location')
        print location, type(location), '!!', [location]
        return TranSugBase.add_view(self,request,form_url,extra_context)






