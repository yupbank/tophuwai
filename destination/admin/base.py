#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib import admin
from destination.models.base import Modify, Tag, SuggestMonth, Transport
from destination.models import Place, Province, Area, Line, Tag
from django.contrib.admin.util import unquote
from destination.util import pid_city, pid_province, place_name
from django.contrib.admin.views.main import ChangeList
from django.shortcuts import render_to_response

from django.conf.urls.defaults import patterns

import json

type_fileds = ['tags', 'area', 'tags__isnull', 'lat__isnull', 'image__isnull', 'line']
loc_fileds = ['location', 'city_id', 'province_id']

DISPLAT_NAME = {
    'tags': lambda x: '标签:%s'%Tag.objects.get(id=x),
    'area': lambda x: '景区:%s'%Area.objects.get(id=x),
    'tags__isnull': lambda x: u'无标签' if x else u'有标签',
    'lat__isnull':  lambda x: u'无gps' if x else u'有gps',
    'image__isnull': lambda x: u'无照片' if x else u'有照片',
    'location': lambda x: place_name(int(x)),
    'city_id': lambda x: place_name(int(x)),
    'province_id': lambda x:place_name(int(x)),
    'line': lambda x: '线路:%s'%Line.objects.get(id=x),
}

class MyChangeList(ChangeList):
    res = None
    def get_query_set(self):
        qs = super(MyChangeList, self).get_query_set()
        if qs.count() < 200:
            self.res = qs
        return qs

class BaseAdmin(admin.ModelAdmin):
    actions_on_top = False
    actions_on_bottom = True
    actions = ['default_action']
    res = None

    def get_urls(self):
        urls = super(BaseAdmin, self).get_urls()
        my_urls = patterns('',
            (r'^showmap/$', self.map_view),
        )
        return my_urls + urls

    def map_view(self, request):
        data = request.POST
        return render_to_response('admin/show_map.html', data)

    def get_changelist(self, request, **kwargs):
        return MyChangeList


    def changelist_view(self, request, extra_context={}):
        list_type_name = None
        list_loc_name = None
        for list_type in type_fileds:
            list_type_name = request.GET.get(list_type)
            if list_type_name:
                list_type_name = DISPLAT_NAME[list_type](list_type_name)
                break
        for list_loc in loc_fileds:
            list_loc_name = request.GET.get(list_loc)
            if list_loc_name:
                list_loc_name = DISPLAT_NAME[list_loc](list_loc_name)
                break

        list_display = list(self.list_display)
        actions = self.get_actions(request)

        if not actions:
            try:
                list_display.remove('action_checkbox')
            except Exception, e:
                pass



        ChangeList = self.get_changelist(request)
        try:
            cl = ChangeList(request, self.model, list_display, self.list_display_links,
                self.list_filter, self.date_hierarchy, self.search_fields,
                self.list_select_related, self.list_per_page, self.list_editable, self)
        except Exception, e:
            print e
            cl = None
        search_loc = []
        if cl and cl.res:
            if cl.res.count() < 200:
                for i in cl.res.all():
                    if hasattr(i, 'lat'):
                        search_loc.append([i.id, i.full_name, [i.lat, i.lng]])
        extra_context.update(dict(list_type_name= list_type_name, list_loc_name=list_loc_name, search_loc=json.dumps(search_loc)))
        return admin.ModelAdmin.changelist_view(self, request, extra_context)

    def default_action(self, request, queryset):
        hot = request.POST.get('hot')
        if hot:
            queryset.update(hot=hot)
        suggest_time = request.POST.get('suggest_time')
        if suggest_time:
            queryset.update(suggest_time=suggest_time)
        tags = request.POST.get('tags')
        for tag_name in tags.strip().replace(u'，', ',').replace(' ', ',').split(','):
            if tag_name:
                tag = Tag.get_or_create_by_name(tag_name.strip(), request.user)
                for obj in queryset:
                    if not obj.tags.filter(tags=tag):
                        obj.tags.add(tag)


    default_action.short_description = '----------'

    #def response_change(self, request, obj):
    #    if request.POST.get('next_link'):
    #        return HttpResponseRedirect(request.POST.get('next_link'))
    #    else:
    #        return admin.ModelAdmin.response_change(self, request, obj)
    def response_change(self, request, obj):
        if '_add_close' in request.POST:
            return HttpResponse("<script language:javascript>javascript:window.opener=null;window.open('','_self');window.close();</script>")
        else:
            return admin.ModelAdmin.response_change(self, request, obj)
    
    def response_add(self, request, obj):
        if '_add_close' in request.POST:
            return HttpResponse("<script language:javascript>javascript:window.opener=null;window.open('','_self');window.close();</script>")
        else:
            return admin.ModelAdmin.response_add(self, request, obj)

    def change_view(self, request, object_id, extra_context={}):
        next_link = request.META.get('HTTP_REFERER')
        obj = self.get_object(request, unquote(object_id))
        tags = obj.tags.all()
        tag_names = [ i.tag for i in tags ]
        extra_context.update(dict(tag_names=tag_names, next_link=next_link))
        return admin.ModelAdmin.change_view(self, request, object_id, extra_context)

    def save_model(self, request, obj, form, change):

        tags_name = request.POST.get('tags', '')
        tags_name = tags_name.strip().replace(u'，', ',').replace(' ', ',')
        tags_name = tags_name.split(',')
        user = request.user
        if not hasattr(obj, 'creater'):
            obj.creater = request.user
        mod = Modify(modify_user=user)
        mod.save()
        obj.modify = mod
        obj.save()
        obj.tags.clear()
        for name in tags_name:
            name = name.strip()
            if name:
                tag = Tag.get_or_create_by_name(name.strip(), request.user)
                obj.tags.add(tag)
        self.save_province(obj)

        obj.save()

    def save_province(self, obj):
        if hasattr(obj, 'location'):
            province_id = pid_province(obj.location)
            province = Province.objects.get(value=province_id)
            if hasattr(obj, 'province_set'):
                p = obj.province_set.all()
                if p:
                    p = p[0]

            if isinstance(obj, Place):
                if not province.places.filter(id=obj.id).exists():
                    if p:
                        p.places.remove(obj)
                    #Province.remove_place(obj)
                    province.places.add(obj)
            elif isinstance(obj, Line):
                if not province.lines.filter(id=obj.id).exists():
                    if p:
                        p.lines.remove(obj)
                    #Province.remove_line(obj)
                    province.lines.add(obj)
            elif isinstance(obj, Area):
                if not province.areas.filter(id=obj.id).exists():
                    if p:
                        p.areas.remove(obj)
                    #Province.remove_area(obj)
                    province.areas.add(obj)
            province.modify = obj.modify
            province.save()

    def delete_model(self, request, obj):
        if not isinstance(obj, Line):
            province = Province.objects.get(value=obj.province_id)
        else:
            province = obj.places.all()
            province = Province.objects.filter(value__in=set([i.province_id for i in province]))
        if isinstance(obj, Place):
            if obj in province.places.filter(pk=obj.id):
                province.places.remove(obj)
        elif isinstance(obj, Line):
            for p in province:
                o = p.lines.remove(obj)

                province.lines.remove(obj)
        elif isinstance(obj, Area):
            if  obj in province.areas.all():
                province.areas.remove(obj)
                province
        province.save()
        return admin.ModelAdmin.delete_model(self, request, obj)


class TranSugBase(BaseAdmin):
    def common_data(self):
        suggest_months = SuggestMonth.objects.all()
        transports = Transport.objects.all()
        extra_context = dict(
            suggest_months=suggest_months,
            transports=transports,
        )
        return extra_context

    def add_view(self, request, form_url='', extra_context={}):
        extra_context.update(self.common_data())
        return BaseAdmin.add_view(self, request, form_url, extra_context)

    def change_view(self, request, object_id, extra_context={}):
        obj = self.get_object(request, unquote(object_id))
        choosen_months = obj.suggest_month.all()
        choosen_month_values = [ i.value for i in choosen_months]
        choosen_transports = obj.transport.all()
        choosen_transport_values = [i.value for i in choosen_transports]

        extra_context.update(dict(
            choosen_month_values = choosen_month_values,
            choosen_transport_values=choosen_transport_values
        ))
        extra_context.update(self.common_data())
        return BaseAdmin.change_view(self, request, object_id, extra_context)

    def save_model(self, request, obj, form, change):
        BaseAdmin.save_model(self, request, obj, form, change)
        transport = request.POST.getlist('transport')
        suggest_month = request.POST.getlist('suggest_month')
        obj.save()
        obj.transport.clear()
        for i in transport:
            obj.transport.add(Transport.objects.get(value=i))
        obj.suggest_month.clear()
        for i in suggest_month:
            obj.suggest_month.add(SuggestMonth.objects.get(value=i))


