# coding: utf-8

__author__ = 'yupbank'
from django.contrib import admin
from base import TranSugBase
from destination.util import pid_city, pid_province, place_name, place_full_name
from destination.models import Scene, Tag, Area, Place, Province


class AreaAdmin(TranSugBase):
    fields = ['suggest_month','location','full_name', 'short_name', 'alternative_name',\
              'hot', 'image', 'desc', 'remark',\
              'lat', 'lng', 'altitude', 'suggest_time',]
    list_display = ['id', 'name', 'division', 'gps', 'tag', 'relate_scene', 'hot', 'suggest_time', 'my_modify', 'create', 'operation']
    search_fields = ['full_name', 'short_name', 'scene__full_name']
    actions = ['default_action', 'change_into_place']
    def changelist_view(self, request, extra_context={}):
        #print request.META.get()
        return super(AreaAdmin,self).changelist_view(request, extra_context)


    def queryset(self, request):
        qs = super(AreaAdmin, self).queryset(request)
        #if self.cl.query:
        #    print qs.count(), '!!'
        return qs
    def change_into_place(self, request, queryset):
        for obj in queryset:
            obj_info = obj.terminate()
            p = Place(**obj_info)
            if not Place.objects.filter(full_name=p.full_name).exists():
                p.save()
                for tag in obj.tags.all():
                    p.tags.add(tag)
                for suggest_month in obj.suggest_month.all():
                    p.suggest_month.add(suggest_month)
                for transport in obj.transport.all():
                    p.transport.add(transport)
                self.save_province(p)
            pro = obj.province_set.all()
            if pro:
                pro = pro[0]
                pro.areas.remove(obj)
            #Province.remove_area(obj)
            obj.delete()

    def operation(self, obj):
        return """
        <a href="./%s/" target="_blank">编辑</a>&nbsp;<a href="../scene/add/?area=%s" target="_blank">添加景点</a>
        """%(obj.id, obj.id)
    operation.short_description = u'操作'
    operation.allow_tags = True

    def relate_scene(self, obj):
        count = Scene.objects.filter(area=obj).count()
        return """
        <a href="../scene/?area=%s">%s景点</a>
        """%(obj.id, count)
    relate_scene.short_description = u'关联景点'
    relate_scene.allow_tags = True

    def gps(self, obj):
        return """
            %s,<br />%s,<br />%s
            """%(obj.lat, obj.lng, obj.altitude)
    gps.short_description = u'GPS'
    gps.allow_tags = True

    def tag(self, obj):
        tags = obj.tags.all()
        tags = ['<a href="?tags=%s">%s</a>'%(i.id, i.tag) for i in tags]
        return """
            %s
            """%(','.join(tags))
    tag.short_description = u'主题'
    tag.allow_tags = True

    def create(self, obj):
        return """
            %s<br />%s
            """%(str(obj.create_time), obj.creater)
    create.short_description = u'添加日期'
    create.allow_tags = True
    create.admin_order_field = 'create_time'

    def name(self, obj):
        return """
            %s<br />%s<br />%s
            """%(obj.full_name, obj.short_name or '-', obj.alternative_name or '-')
    name.short_description = u'全称、简称、别名'
    name.allow_tags = True
    name.admin_order_field = 'full_name'


    def division(self, obj):
        province_name = place_name(pid_province(obj.location))
        city_name = place_name(pid_city(obj.location)).replace(province_name, '').strip()
        return """<strong>%s</strong><br />
                    %s
            """%(place_name(pid_province(obj.location)), place_full_name(obj.location).replace(province_name+'-', ''))
    division.short_description = u'行政区划'
    division.allow_tags = True

    def my_modify(self, obj):
        return """
            %s<br />%s
            """%(str(obj.modify.modify_time), obj.modify.modify_user)
    my_modify.short_description = u'最后修改'
    my_modify.allow_tags = True
    my_modify.admin_order_field = 'modify'
