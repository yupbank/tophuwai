#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
gps_province.py
Author: yupbank
Email:  yupbank@gmail.com

Created on
2012-07-28
'''

from django.core.management.base import BaseCommand
from optparse import make_option
import json
import platform
from destination.models import Place, Area 
from destination.util.earth import NAME2PID, NAME2PID_BY_PROVINCE, place_name
from django.contrib.auth.models import User
import urllib
import time

class Command(BaseCommand):
    option_list = BaseCommand.option_list + (
        make_option('-p', '--place', action='store_true', default=False, dest='place', help=u'加载地点GPS'),
        make_option('-a', '--area', action='store_true', default=False, dest='area', help=u'加载景区GPS'),
        make_option('-A', '--all', action='store_true', default=False, dest='all', help=u'加载景区GPS'),
        make_option('-n', '--name', action='store',  dest='name', help=u'加载某省地点GPS'),
        make_option('-c', '--city_id', action='store',  dest='city_id', help=u'加载某cityGPS'),
        make_option('-i', '--place_id', action='store',  dest='place_id', help=u'加载某idGPS'),
        make_option('-l', '--city_id_list', action='store',  dest='city_id_list', help=u'加载city_id_listGPS'),
        )
    
    
    def return_add(self, gps):
        api = 'http://ditu.google.cn/maps/geo?output=json&key=AIzaSyCQP1O5vJmsG0p2Z1vjAtzLDgC7WWnhnpo&hl=zh_cn&q=%s,%s'
        print api%(gps[0], gps[1])
        res = urllib.urlopen(api%(gps[0], gps[1])).read()
        time.sleep(0.1)
        res = json.loads(res)
        names = self.dict_name(res)
        if isinstance(names, tuple):
            location_name = names[0]
            province_name = names[1]
            province_name = province_name.replace(u'省', '').replace(u'市', '').replace(u'维吾尔自治区', '').replace(u'自治区', '')
            province_id = NAME2PID.get(province_name.encode('u8'))
            NAME_DICT = NAME2PID_BY_PROVINCE(province_id)
            names = names[2:]
            error, res = False, []
            for name in names:
                name = name or ' '
                name = name.replace(u'省', '').replace(u'上海市', u'上海')
                pid = None
                if name:
                    print name, '******'
                    try:
                        pid = NAME_DICT.get(name.encode('U8'))
                        print pid, '----***'
                    except Exception:
                        print name, 'error, not exists'
                res.append([name, pid])
                print name, pid 
        else:
            print gps
            error = True
            location_name = names
        return error, location_name, res

    def dict_name(self, f):
        country = f['Placemark'][0]['AddressDetails'].get('Country')
        location_name =  f['Placemark'][0]['address']
        if country:
            province = country.get('AdministrativeArea')
            if province:
                province_name = province['AdministrativeAreaName']
                city_name = province.get('Locality', {}).get('LocalityName')
                couty_name = province.get('Locality', {}).get('DependentLocality', {}).get('DependentLocalityName')
                return location_name, province_name, city_name, couty_name
            else:
                province_name = country.get('Locality', {}).get('LocalityName')
                dis_name = country.get('Locality', {}).get('DependentLocality', {}).get('DependentLocalityName')
                return location_name, province_name, dis_name
        else:
            #print f['Placemark'][0]['address'], '!!!'
            #print f['Placemark'][0]
            #print f['Placemark'][0]['AddressDetails']['Locality']['LocalityName']
            print f['Placemark'][0]['address']
            return f['Placemark'][0]['address']

    def save_place(self):
        print 'place'
        pass

    def save_area(self):
        print 'area'
        for area in Area.objects.all():
            try:
                #if area.location != area.province_id:
                #    print area, area.id, 'already done!'
                #else:
                gps = area.lat, area.lng
                if all(gps):
                    error, location_name, res = self.return_add(gps)
                    area.location_name = location_name
                    if not error:
                        name, location = res[-1]
                        if location:
                            area.location = location
                        else:
                            print '****error', name, location
                        print area, area.id, name, location, 'success!'
                    area.save()
            except Exception:
                time.sleep(1)
                continue
            print '-------------'
     
    def handle(self, *args, **options):
        load_place = options['place']
        load_area = options['area']
        load_all = options['all']
        name = options.get('name')
        city_id = options.get('city_id')
        place_id = options.get('place_id')
        city_id_list = options.get('city_id_list')
        if city_id_list:
            city_id_list = city_id_list.strip().split(',')
            for city_id in city_id_list:
                if city_id:
                    for area in Area.objects.filter(city_id=city_id):
                        gps = area.lat, area.lng
                        if all(gps):
                            try:
                                error, location_name, res = self.return_add(gps)
                                area.location_name = location_name
                                if not error:
                                    name, location = res[-1]
                                    if location:
                                        area.location = location
                                    else:
                                        print '****error', name, location
                                    print area, area.id, name, location, 'success!'
                                area.save()
                            except Exception:
                                time.sleep(1)
                                continue



        if name and NAME2PID.get(name):
            for area in Area.objects.filter(province_id=NAME2PID[name]):
                gps = area.lat, area.lng
                if all(gps):
                    try:
                        error, location_name, res = self.return_add(gps)
                        area.location_name = location_name
                        if not error:
                            name, location = res[-1]
                            if location:
                                area.location = location
                            else:
                                print '****error', name, location
                            print area, area.id, name, location, 'success!'
                        area.save()
                    except Exception:
                        time.sleep(1)
                        continue
        if city_id:
            for area in Area.objects.filter(city_id=city_id):
                gps = area.lat, area.lng
                if all(gps):
                    try:
                        error, location_name, res = self.return_add(gps)
                        area.location_name = location_name
                        if not error:
                            name, location = res[-1]
                            if location:
                                area.location = location
                            else:
                                print '****error', name, location
                            print area, area.id, name, location, 'success!'
                        area.save()
                    except Exception:
                        time.sleep(1)
                        continue




        if place_id:
            for area in Area.objects.filter(id=place_id):
                gps = area.lat, area.lng
                if all(gps):
                    try:
                        error, location_name, res = self.return_add(gps)
                        area.location_name = location_name
                        print res, '#####'
                        for i,j in res:
                            print place_name(j), j, i
                        if not error:
                            name, location = res[-1]
                            if location:
                                area.location = location
                            else:
                                print '****error', name, location
                            print area, area.id, name, location, 'success!'
                        area.save()
                    except Exception:
                        time.sleep(1)
                        continue
        if load_area:
            self.save_area()
        if load_place:
            self.save_place()
        if load_all:
            self.save_area()
            self.save_place()
