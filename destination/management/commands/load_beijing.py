#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
load_beijing.py
Author: yupbank
Email:  yupbank@gmail.com

Created on
2012-07-24
'''

from django.core.management.base import BaseCommand
from optparse import make_option
import json
import platform
from destination.models import Place, Transport, SuggestMonth, Modify, Province,Area, Tag, Scene
from destination.util.earth import NAME2PID
from django.contrib.auth.models import User

class Command(BaseCommand):
    option_list = BaseCommand.option_list + (
        make_option('-s', '--scene', action='store_true', default=False, dest='scene', help=u'加载景点'),
        make_option('-a', '--area', action='store_true', default=False, dest='area', help=u'加载景区'),
        make_option('-A', '--all', action='store_true', default=False, dest='all', help=u'顺序加载')
        )
    if platform.node() == 'vps136':
        data_position = '/home/huwai/beijing/'
    else:
        data_position = '/Users/yupbank/projects/beijing/'
    
    def return_data(self, name):
        with open('%s%s'%(self.data_position, name)) as f:
            f = f.read()
            f = f.replace('null', 'None')
            f = eval(f)
            return f
   
    province = Province.objects.get(id=29)
    def save_area(self):
        for i in self.return_data('jingqu'):
            if not Area.objects.filter(full_name=i['name']).exists():
                a = Area(full_name=i['name'].decode('u8'))
            else:
                a = Area.objects.get(full_name=i['name'])
            a.lat = i['lat']
            a.lng = i['lng']
            a.location_name = i['location_name']
            a.hot = i['hot']
            a.suggest_time = i['suggest_time']

            city_id = NAME2PID['北京']

            if i.get('district_name'):
                city_id = NAME2PID.get(i.get('district_name'))
            a.location = city_id
            if i.get('short_name'):
                a.short_name = i.get('short_name', '')
            creater = User.objects.get(pk=1)
            modify = Modify(modify_user=creater)
            modify.save()
            a.creater = creater
            a.modify = modify
            a.save()
            tags = i.get('tags')
            if tags:
                tags = tags.split(',')
                tags = [self.save_tag(i.decode('u8'),creater) for i in tags]
                for tag in tags:
                    a.tags.add(tag)
            a.save()
            self.province.areas.add(a)
            print a, 'success!'
    def save_tag(self, tag_name, creater):
        tag = Tag.get_or_create_by_name(tag_name, creater)
        return tag
        
    def save_scene(self):
        for i in self.return_data('jingdian'):
            try:
                a = Area.objects.get(full_name=i['area'].decode('u8'))
            except :
                print i['area'], i['name'], 'error'
                continue
            if not Scene.objects.filter(full_name=i['name']).exists():
                s = Scene(full_name=i['name'].decode('u8'))
            else:
                s = Scene.objects.get(full_name=i['name']) 
            city_id = NAME2PID['北京']

            if i.get('district_name'):
                city_id = NAME2PID.get(i.get('district_name'))
            s.location = city_id

            s.lat = i['lat']
            s.lng = i['lng']
            s.altitude = i['altitude']
            s.hot = i['hot']
            if i.get('short_name'):
                s.short_name = i.get('short_name', '')
            s.suggest_time = i['suggest_time']
            creater = User.objects.get(pk=1)
            modify = Modify(modify_user=creater)
            modify.save()
            s.creater = creater
            s.modify = modify
            s.area = a
            s.save()
            tags = i.get('tags')
            if tags:
                tags = tags.split(',')
                tags = [self.save_tag(i.decode('u8'),creater) for i in tags]
                for tag in tags:
                    s.tags.add(tag)
            s.save()
            print s, 'success!!'

    def handle(self, *args, **options):
        load_scene = options['scene']
        load_area = options['area']
        load_all = options['all']
        if load_area:
            self.save_area()
        if load_scene:
            self.save_scene()
        if load_all:
            self.save_area()
            self.save_scene()
        
