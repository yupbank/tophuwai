# coding: utf-8

__author__ = 'yupbank'
from django.core.management.base import BaseCommand
from optparse import make_option
import json
import glob
from destination.util.earth import PROVINCE_NAME_ID
from destination.models import Place, Transport, SuggestMonth, Modify, Province,Area
from django.contrib.auth.models import User
import logging
import platform

logger = logging.getLogger(__file__)

class Command(BaseCommand):
    option_list = BaseCommand.option_list + (
        make_option('-a', '--all', action='store_true', dest='load_all', help=u'加载所有文件'),
        make_option('-n', '--name', action='store', default=u'重庆', dest='name', help=u'显示数量'),
        )
    if platform.node() == 'vps136':
        data_position = '/home/huwai/end/'
    else:
        data_position = '/Users/yupbank/projects/end/'


    def return_file(self, name=''):
        if name:
            with open('%s%s'%(self.data_position, name)) as f:
                f = f.read()
                yield json.loads(f)
        else:
            for i in glob.glob('%s*'%self.data_position):
                place_name = i.strip().split('/')[-1]
                with open(i) as f:
                    f = f.read()
                    yield place_name.decode('u8'), json.loads(f)

    def save_data(self, data, location, location_name):
        for name, value in data.iteritems():
            full_name = name
            gps = value.get('taobao_gps') or value.get('mafengwo_gps') or [0,0]
            try:
                lat, lng = gps
                lat, lng = float(lat), float(lng)
            except Exception:
                print lat, name
                logger.error('%s-%s-%s'%(location_name, name, gps))
                print 'error~~%s-%s-%s'%(location_name, name, gps)
                lat, lng = 0,0
            trans = Transport.objects.get(pk=1)
            suggest_time = 1
            suggest_month = SuggestMonth.objects.get(pk=1)
            if not Area.objects.filter(full_name=full_name).exists():
                p = Area()
                p.lat = lat
                p.lng = lng
                p.full_name = full_name
                p.suggest_time =suggest_time
                p.location = location
                creater = User.objects.get(pk=1)
                p.creater = creater
                p.location_name = location_name
                modify = Modify(modify_user=creater)
                modify.save()
                hot = 1
                p.hot =hot
                p.modify = modify
                p.save()
                p.suggest_month.add(suggest_month)
                p.transport.add(trans)
                p.save()
                my_province = Province.objects.get(value=location)
                if not my_province.areas.filter(id=p.id).exists():
                    my_province.areas.add(p)
                    my_province.save()
                print name, 'success!'
            else:
                print name, 'exists!'

    def handle(self, *args, **options):
        self.name = options['name']
        self.all = options['load_all']
        if self.all:
            for place_name, data in self.return_file():
                location =  PROVINCE_NAME_ID.get(place_name)
                self.save_data(data, location, place_name )
        elif self.name:
            for data in self.return_file(self.name):
                location =  PROVINCE_NAME_ID.get(self.name)
                self.save_data(data, location, self.name)
