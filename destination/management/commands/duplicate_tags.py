#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
duplicate_tags.py
Author: yupbank
Email:  yupbank@gmail.com

Created on
2012-08-13
'''

from django.core.management.base import BaseCommand
from optparse import make_option
import json
from destination.models import Place, Transport, SuggestMonth, Modify, Province,Area, Tag, Scene


class Command(BaseCommand):
    #option_list = BaseCommand.option_list + (
    #    make_option('-s', '--scene', action='store_true', default=False, dest='scene', help=u'加载景点'),
    #    make_option('-a', '--area', action='store_true', default=False, dest='area', help=u'加载景区'),
    #    make_option('-A', '--all', action='store_true', default=False, dest='all', help=u'顺序加载')
    #    )
    
    
    def handle(self, *args, **options):
        pure_names = set([i.tag.strip() for i in Tag.objects.all()])
        pure_ids = [ int(Tag.objects.get(tag=j).id) for j in pure_names]
        all_ids = [ int(i.id) for i in Tag.objects.all()]
        del_ids = [ i for i in all_ids if i not in pure_ids]
        print len(pure_ids), len(all_ids), len(del_ids), del_ids
        for del_id in del_ids:
            del_tag = Tag.objects.get(id=del_id)
            real_tag = Tag.objects.get(tag=del_tag.tag.strip())
            print del_tag.id, del_tag.tag
            for area in del_tag.area_set.all():
                area.tags.add(real_tag)
            del_tag.delete()
            
            

        
