#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
send_mail.py
Author: yupbank
Email:  yupbank@gmail.com

Created on
2012-09-07
'''

from django.core.management.base import BaseCommand
from optparse import make_option


SENDER_NAME = u'与'
SENDER_MAIL = 'admin@tophuwai.com'
NOEMAIL = 'yupbank@qq.com'
SMTP = 'smtp.mailgun.org'
SMTP_USERNAME = 'postmaster@tophuwai.mailgun.org'
SMTP_PASSWORD = '6qmonsfu01h3'
NOT_SUPPORT_UTF8_DOMAIN = set(['tom.com', 'hotmail.com', 'msn.com', 'yahoo.com'])

class Command(BaseCommand):
    option_list = BaseCommand.option_list + (
        make_option('-p', '--place', action='store_true', default=False, dest='place', help=u'加载地点GPS'),
        )
     
    def sendmail_imp(
            self,
            smtp,
            sender, sender_name,
            recipient, recipient_name,
            subject, body, enc='utf-8',
            format='plain'
        ):
        if not subject:
            return

        at = recipient.find('@')
        if at <= 0:
            return

        domain = recipient[at+1:].strip()
        if domain not in NOT_SUPPORT_UTF8_DOMAIN:
            enc = 'utf-8'
        else:
            enc = 'gb18030'

        if enc.lower() != 'utf-8':
            sender_name = ignore_encode(sender_name, enc)
            recipient_name = ignore_encode(recipient_name, enc)
            body = ignore_encode(body, enc)
            subject = ignore_encode(subject, enc)

        msg = MIMEText(body, format, enc)
        msg['Subject'] = Header(subject, enc)

        sender_name = str(Header(sender_name, enc))
        msg['From'] = formataddr((sender_name, sender))

        recipient_name = str(Header(recipient_name, enc))
        msg['To'] = formataddr((recipient_name, recipient))

        smtp.sendmail(sender, recipient, msg.as_string())
    
    def sendmail(
            self,
            subject,                                                                  
            text, email, name=None, sender=SENDER_MAIL,                               
            sender_name=SENDER_NAME,                                                  
            format='plain'                                                            
        ):                                                                            
        if not email:                                                             
            email = NOEMAIL                                                       
            subject = '->%s : %s'%(name, subject)                                 
                                                                                  
        if name is None:                                                          
            name = email.rsplit('@', 1)[0]                                        
        server = smtplib.SMTP(SMTP)                                               
        server.ehlo()                                                             
        server.esmtp_features['auth'] = 'LOGIN PLAIN'                             
        server.login(SMTP_USERNAME, SMTP_PASSWORD)                                
                                                                                  
        #print email                                                              
        if type(text) is unicode:                                                 
            text = text.encode('utf-8', 'ignore')                                 
        if type(subject) is unicode:                                              
            subject = subject.encode('utf-8', 'ignore')                           
        self.sendmail_imp(server, sender, sender_name, email, name, subject, text, format=format)
       #if email != NOEMAIL:                                                     
       #    subject = '%s %s %s'%(name, subject, email)                          
       #    sendmail_imp(server, sender, sender_name, 'kanrss_backup@googlegroups    .com', name, subject, text, format=format)
                                                                                  
        server.quit()  
     
    def handle(self, *args, **options):
        self.sendmail('heihei', '你好啊', 'yupbank@gmail.com')
        print 'ok'
        #load_place = options['place']

