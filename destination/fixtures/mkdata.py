#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
mkdata.py
Author: yupbank
Email:  yupbank@gmail.com

Created on
2012-07-17
'''
import json
from province import get_res

def main():
    value_list = [
                    (1, u'一月'),
                    (2, u'二月'),
                    (3, u'三月'),
                    (4, u'四月'),
                    (5, u'五月'),
                    (6, u'六月'),
                    (7, u'七月'),
                    (8, u'八月'),
                    (9, u'九月'),
                    (10, u'十月'),
                    (11, u'十一月'),
                    (12, u'十二月'),
            ]
    res = []
    for value, name in value_list:
        res.append(dict(model="destination.SuggestMonth", pk=value, fields=dict(value=value, name=name)))

    trans_list = [
            (1, u'汽车'),
            (2, u'火车'),
            (3, u'飞机'),
            (4, u'徒步')
            ]
    for value, trans in trans_list:
        res.append(dict(model="destination.Transport", pk=value, fields=dict(name=trans, value=value)))
    res.extend(get_res())
    print json.dumps(res)

if __name__ == '__main__':
    main()
