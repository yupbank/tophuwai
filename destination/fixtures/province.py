#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
province.py
Author: yupbank
Email:  yupbank@gmail.com

Created on
2012-07-18
'''
import json
import sys
sys.path.append('/Users/yupbank/projects/tophuwai')

from destination.util import PLACE_MUNI, PLACE_CITY_L1, place_name

def get_res():
    places = []
    places.extend(PLACE_CITY_L1)
    places.extend(PLACE_MUNI)
    res = []
    for num, place in enumerate(places):
        res.append(dict(model="destination.Province", pk=num+1, fields=dict(name=place_name(place), value=place)))
    return res
