# coding: utf-8

from django import forms
from models import Area, Line, Place, Scene

class PlaceForm(forms.ModelForm):
    def clean(self):
        return super(self, AreaForm).clean()

    class Meta(object):
        models = Place
        widgets = {            
                'suggest_month': forms.CheckboxSelectMultiple(choices=(
                        (1, u'一月'),
                        (2, u'二月'),
                        (3, u'三月')
                    )),
                'location': forms.HiddenInput(),
                'transport': forms.CheckboxSelectMultiple(),


                }
class SceneForm(forms.ModelForm):
    class Meta(object):
        models = Scene
        widgets ={
            'area': forms.HiddenInput(),

        }

#class PictureGroupForm(forms.ModelForm):
#    class Meta(object):
#        models = PictureGroup
#        widgets = {
#            'title': forms.TextInput(attrs={'style':'width:250px;'}),
#            'id': forms.TextInput(attrs={'style':'width:250px;'}),
#            'channel': forms.Select(attrs={'style':'width:250px;'}),
#            'thumb_image_urls': forms.Textarea(attrs={'style': 'width:75%;height:90px;'}),
#            'image_urls': forms.Textarea(attrs={'style': 'width:75%;height:100px;'}),
#            }
#
#class TemplateForm(forms.ModelForm):
#    class Meta(object):
#        models = Template
#        widgets = {
#            'name': forms.TextInput(attrs={'size':"20"}),
#            'channel': forms.HiddenInput(),
#            'type': forms.RadioSelect(),
#            'update_time': forms.TextInput(attrs={'style':'width:250px;'}),
#            'thumb_url': forms.TextInput(attrs={'style':'width:250px;'}),
#            'content': forms.Textarea(attrs={'style':'width:75%;;height:300px;'}),
#            'desc': forms.TextInput(attrs={'size':'80'}),
#            }
#
#class DynamicFragmentForm(forms.ModelForm):
#
#    class Meta(object):
#        models = DynamicFragment
#        widgets = {
#            'name': forms.TextInput(attrs={'style':'width:250px;'}),
#            'template': forms.Select(attrs={'style':'width:250px;'}),
#            'channel': forms.Select(attrs={'style':'width:250px;'}),
#            'data_channel': forms.Select(attrs={'style':'width:250px;'}),
#            'size': forms.TextInput(attrs={'style':'width:250px;'}),
#            #  'content': forms.Textarea(attrs={'style':'displat:None'}),
#            'desc': forms.Textarea(attrs={'style': 'width:75%;height:50px; '})
#        }
#
#class StaticFragmentForm(forms.ModelForm):
#    class Meta(object):
#        models = StaticFragment
#        widgets = {
#            'name': forms.TextInput(attrs={'style':'width:250px;'}),
#            'template': forms.Select(attrs={'style':'width:250px;'}),
#            'channel': forms.Select(attrs={'style':'width:250px;'}),
#            'json_type': forms.Select(attrs={'style':'width:250px;'}),
#            'json': forms.Textarea(attrs={'style': 'width:75%;height:250px;'}),
#            #  'content': forms.Textarea(attrs={'style':'displat:None'}),
#            'desc': forms.Textarea(attrs={'style': 'width:75%;height:50px;'}),
#
#            }
#
#class ChannelForm(forms.ModelForm):
#    class Meta(object):
#        models = Channel
#        widgets = {
#            'name': forms.TextInput(attrs={'style':'width:250px;'}),
#            'path': forms.TextInput(attrs={'style':'width:250px;'}),
#            'w3channels': forms.TextInput(attrs={'style':'width:250px;'}),
#            'parent': forms.Select(attrs={'style':'width:250px;'}),
#            'real': forms.Select(attrs={'style':'width:250px;'}),
#            'content_type': forms.RadioSelect(),
#            'desc': forms.Textarea(attrs={'style': 'width:75%;height:50px;'})
#        }
#    def clean(self):
#        if u'template' in self.data.keys():
#            _template = self.data.get(u'template', None)
#            if not _template:
#                self._errors["template"] = u'必填字段'
#        if u'parent' in self.data.keys():
#            if not self.cleaned_data.get("parent"):
#                self._errors["parent"] = u'必选字段'
#
#        w3channels = self.data.get('w3channels', None)
#        channel_id = self.data.get('channel_id', None)
#
#        if w3channels:
#            for channel in w3channels.split(','):
#                if channel_id:
#                    if Channel.objects.exclude(id=channel_id).filter(w3channels__contains=channel).exists():
#                        channels = Channel.objects.exclude(id=channel_id).filter(w3channels__contains=channel)
#                        names = '|'.join([c.full_name for c in channels])
#                        self._errors['w3channels'] = u'该WWW频道已经添加到了频道: %s www频道: %s' % (names, channel)
#                        break
#            #return self.cleaned_data
#        return self.cleaned_data
#
#class PictureForm(forms.ModelForm):
#    class Meta(object):
#        models = Picture
#        widgets = {
#            'title': forms.TextInput(attrs={'style':'width:250px;'}),
#            }
