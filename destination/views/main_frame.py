#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
main_frame.py
Author: yupbank
Email:  yupbank@gmail.com

Created on
2012-07-07
'''
import logging                                                                    
from django.contrib.auth.decorators import login_required
from .base import get_session_and_user, render_with_session, get_session
logger = logging.getLogger(__name__)


#@login_required(login_url='login/')
def main_frame_view(request):
    session = get_session(request)
    data = {
        'user': request.user,
        's': session.session_id,
    }                                                                             
                                                                                  
    return render_with_session('main/main.html', data, session)
