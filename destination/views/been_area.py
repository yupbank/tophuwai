from destination.models import BeenArea, Area, User
from .base import get_session_and_user, render_with_session, login_required
from django.http import HttpResponse

import json


@login_required
def been_view(request):
    session, user = get_session_and_user(request)
    ba = BeenArea.objects.get(user=user)
    bas = ba.get_been_area()
    res = Area.get_name_by_ids(bas)
    return HttpResponse(json.dumps(res))

@login_required
def add_view(request):
    session, user = get_session_and_user(request)
    ba_id = request.GET.get('ba_id')
    if ba_id and Area.objects.get(pk=ba_id).exists():
        ba = BeenArea.objects.get(user=user)
        ba.add_been_area(Area.objects.get(pk=ba_id))
        return HttpResponse(json.dump({'res':'ok'}))
    else:
        return HttpResponse(json.dump({'error':'valid ba_id'}))

@login_required
def delete_view(request):
    session, user = get_session_and_user(request)
    ba_id = request.GET.get('ba_id')
    if ba_id and Area.objects.get(pk=ba_id).exists():
        ba = BeenArea.objects.get(user=user)
        ba.delete_been_area(Area.objects.get(pk=ba_id))
        return HttpResponse(json.dump({'res':'ok'}))
    else:
        return HttpResponse(json.dump({'error':'valid ba_id'}))








