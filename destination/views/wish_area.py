from destination.models import WishArea, Area
from .base import get_session_and_user, login_required
from django.http import HttpResponse
import json


@login_required
def been_view(request):
    session, user = get_session_and_user(request)
    ba = WishArea.objects.get(user=user)
    bas = ba.get_wish_area()
    res = Area.get_name_by_ids(bas)
    return HttpResponse(json.dumps(res))

@login_required
def add_view(request):
    session, user = get_session_and_user(request)
    wa_id = request.GET.get('wa_id')
    if wa_id and Area.objects.get(pk=wa_id).exists():
        wa = WishArea.objects.get(user=user)
        wa.add_wish_area(Area.objects.get(pk=wa_id))
        return HttpResponse(json.dump({'res':'ok'}))
    else:
        return HttpResponse(json.dump({'error':'valid wa_id'}))

@login_required
def delete_view(request):
    session, user = get_session_and_user(request)
    wa_id = request.GET.get('wa_id')
    if wa_id and Area.objects.get(pk=wa_id).exists():
        ba = WishArea.objects.get(user=user)
        ba.delete_wish_area(Area.objects.get(pk=wa_id))
        return HttpResponse(json.dump({'res':'ok'}))
    else:
        return HttpResponse(json.dump({'error':'valid wa_id'}))

