__author__ = 'yupbank'

from destination.models import Tag, Area
from .base import get_session_and_user, render_with_session, get_session
from django.http import HttpResponse
import json




def tags_view(request):
    tag_ids = Area.get_all_tags()
    tags = Tag.objects.filter(pk__in=tag_ids)
    res = [[i.id, i.tag] for i in tags]
    res = json.dumps(res)
    return HttpResponse(res)