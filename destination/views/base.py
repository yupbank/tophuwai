from django.http import HttpResponse

__author__ = 'yupbank'
from destination.util.session import Session
from django.shortcuts import render_to_response
from destination.models import User
import json


def get_session_and_user(request):
    session_id = request.GET.get('s', None)
    session = Session.get_session(session_id)
    user = User.objects.get(pk=session['user_id'])
    return session, user

def get_session(request):
    session_id = request.GET.get('s', None)
    session = Session.get_session(session_id)
    return session

def render_with_session(template_file, data={}, session=None):
    if session:
        data.update({'s':session.session_id})

    return render_to_response(template_file, data)

def login_required(func):
    def _(request, *args, **kwargs):
        session = get_session(request)
        user = User.objects.get(pk=session['user_id'])
        if not user:
            return HttpResponse(json.dumps({'error':'need login first'}))
        else:
            return func(request, *args, **kwargs)
    return _