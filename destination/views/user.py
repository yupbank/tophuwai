#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
main_frame.py
Author: yupbank
Email:  yupbank@gmail.com

Created on
2012-07-07
'''
from django.contrib.auth.decorators import login_required
from .base import render_with_session, get_session_and_user
from destination.models import new_user, login
from django.http import HttpResponse
import json

#@login_required(login_url='login/')
def new_user_view(request):
    session, user = get_session_and_user(request)
    source = request.GET.get('source')
    login_name = request.GET.get('login_name')
    login_pwd = request.GET.get('login_pwd')
    uid = request.GET.get('uid')
    access_token = request.GET.get('access_token')


    user = new_user(cid=source, login_name=login_name, login_pwd=login_pwd, uid=uid, access_token=access_token)
    if not user:
        return HttpResponse(json.dumps({'error':'aleady exists!'}))


    session['user_id'] = user.user_id
    return HttpResponse(json.dumps({'user_id':user.user_id, 's':session.session_id}))


def user_login(request):
    session, user = get_session_and_user(request)
    source = request.GET.get('source')
    login_name = request.GET.get('login_name')
    login_pwd = request.GET.get('login_pwd')
    uid = request.GET.get('uid')
    access_token = request.GET.get('access_token')
    
    user = login(cid, login_name=login_name, login_pwd=login_pwd, access_token=access_token, uid=uid) 
    session['user_id'] = user.user_id
    if user:
        return HttpResponse(json.dumps({'user_id': user.user_id,
                                        's':session.session_id,
                                        }))


def user_logout(request):
    session, user = get_session_and_user(request)
    if user:
        session.clear()
    return HttpResponse(json.dumps({'res': 'ok'}))