#!/bin/bash
set -e
LOGFILE=/root/tophuwai/log/guni.log
LOGDIR=$(dirname $LOGFILE)
NUM_WORKERS=3
# user/group to run as
ADDRESS=127.0.0.1:9000
test -d $LOGDIR || mkdir -p $LOGDIR
exec gunicorn_django -w $NUM_WORKERS --bind=$ADDRESS \
        --log-level=debug \
        --log-file=$LOGFILE 2>>$LOGFILE
