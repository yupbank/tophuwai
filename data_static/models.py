from django.db import models
from destination.models import Tag as T
# Create your models here.

class Tags(T):
    name = models.CharField(max_length=100)
    
    class Meta(object):
        app_label = 'data_static'
