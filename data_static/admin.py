#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
admin.py
Author: yupbank
Email:  yupbank@gmail.com

Created on
2012-07-08
'''
from models import Tags
from django.contrib import admin

from django.contrib.sites.models import Site
admin.site.register(Tags)
