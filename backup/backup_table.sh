PREFIX=$(cd "$(dirname "$0")"; pwd)
FILEDIR=$(dirname "$0")
FILE=$FILEDIR/tophuwai_main_table_`date +%Y_%m_%d`.sql

mysqldump --skip-comments --no-data --default-character-set=utf8 --skip-opt --add-drop-table --create-options --quick --hex-blob -uroot -p1 tophuwai > $FILE
